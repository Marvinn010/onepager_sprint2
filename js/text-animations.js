$(function(){

  $(window).scroll(function(){
    var scroll = $(window).scrollTop();
    if(scroll >= 1150){
      $(".text1").addClass("text1-animation");
      $(".feedback-img").addClass("feedback-img-animation");
    }

    else if (scroll >= 500){
      $(".centered").addClass("text1-animation");
      $(".box1").addClass("box1-animation");
      $(".box2").addClass("feedback-img-animation");
      $(".box3").addClass("box3-animation");

    }
  });
});
