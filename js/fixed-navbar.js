$(function(){
  $(window).scroll(function() {
    var scroll = $(window).scrollTop();

    if (scroll >= 2950){
        $(".color4").css({color: '#585858'});
        $(".color5").css({color: '#f15922'});
    }
    else if (scroll >= 2250){

        $(".color3").css({color: '#585858'});
        $(".color4").css({color: '#f15922'});
        $(".color5").css({color: '#585858'});
    }

    else if (scroll >= 1700){
        $(".color2").css({color: '#585858'});
        $(".color3").css({color: '#f15922'});
        $(".color4").css({color: '#585858'});
    }

    else if (scroll >= 1050){
        $(".color1").css({color: '#585858'});
        $(".color2").css({color: '#f15922'});
        $(".color3").css({color: '#585858'});
    }

    else if (scroll >= 350){
        $(".color1").css({color: '#f15922'});
        $(".color2").css({color: '#585858'});

    }

    else if (scroll >= 300){
      $(".header").fadeIn("10");
      $(".header").attr("id","header-fixed");
      $(".header1").css("display", "none");
      $("body").css("padding-top", "85px");
    }
    else{
        $(".header").fadeOut("fast");
        $(".header1").fadeIn("10");
        $("body").css("padding-top", "0px");
        $(".color1").css({color: '#585858'});
        $(".color2").css({color: '#585858'});
        $(".color3").css({color: '#585858'});
        $(".color4").css({color: '#585858'});
        $(".color5").css({color: '#585858'});
    }
  });
});
